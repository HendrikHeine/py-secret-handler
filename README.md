# pySecretHandler

Secret Handler for API tokens or something like that. It can load secrets from a json or a txt file.

## Usage

Example in `main.py` <br>
On Production, please add the secret files in the `.gitignore` file with `*secret.txt` or `*secret.json`

## Return Values

* 0: Ok
* 1: File could not load
* 2: No item given for JSON
* 3: Invalid file format (for example .exe, .csv)
* 4: No format for file given
* 5: Invalid item for JSON

![Example](media/screenshot.png)