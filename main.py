import secretHandler as sh

secretHanlder = sh.secret()

print("\n----Load valid secrets----")
print(secretHanlder.loadSecret(fileName="secrets/secret_1.txt"))
print(secretHanlder.loadSecret(fileName="secrets/secret_2.txt"))
print(secretHanlder.loadSecret(fileName="secrets/secret.json", item="secret_1"))
print(secretHanlder.loadSecret(fileName="secrets/secret.json", item="secret_2"))

print("\n----Errors----")
print("File load Error:")
print(secretHanlder.loadSecret(fileName="secrets/notValid.txt"))

print("\nInvalid file format:")
print(secretHanlder.loadSecret(fileName="secrets/notValid.noFile"))

print("\nFile without format ending:")
print(secretHanlder.loadSecret(fileName="secrets/notValid"))

print("\nJSON without a item:")
print(secretHanlder.loadSecret(fileName="secrets/secret.json"))

print("\nJSON with a invalid item:")
print(secretHanlder.loadSecret(fileName="secrets/secret.json", item="noValidItem"))

print("\n")
