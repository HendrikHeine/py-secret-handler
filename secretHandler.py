'''https://gitlab.com/HendrikHeine/py-secret-handler'''

import json

class __fileType:
    def __init__(self) -> None:
        pass

    def _loadFromTXT(self, fileName:str):
        '''!!!DONT USE!!!'''
        try:
            with open(file=fileName, mode="r") as file:
                return [0, str(file.readlines()[0].strip("\n"))]
        except:
            return [1]

    def _loadFromJSON(self, fileName:str, item:str):
        '''!!!DONT USE!!!'''
        if item == "":
            return [2]
        else:
            try:
                with open(file=fileName, mode="r") as file:
                    try:return [0, json.loads(file.read())[item]]
                    except:return [5]
            except:
                return [1]

class secret(__fileType):
    '''Class for load secrets like API keys or tokens'''
    def __init__(self) -> None:
        super().__init__()

    def loadSecret(self, fileName:str, item=""):
        '''
        fileName(str)
            the name of the secret file.
            Valid formats: .json and .txt

        item(str)
            name of the item in the JSON file. By default empty
        
        Return Values(list):
            0: Ok
            1: File could not load
            2: No item given for JSON
            3: Invalid file format (for example .exe, .csv)
            4: No format for file given
            5: Invalid item for JSON
        '''
        try:fileType = fileName.split(".")[1]
        except:return [4]

        if fileType == "json":
            result = self._loadFromJSON(fileName=fileName, item=item)
            if result[0] == 0:
                return result
            else:
                return [result[0]]
        
        elif fileType == "txt":
            result = self._loadFromTXT(fileName=fileName)
            if result[0] == 0:
                return result
            else:
                return [result[0]]

        else:
            return [3]
